package Model;

import java.util.LinkedList;
import java.util.List;

public class Scheduler {
	private List<Server> servers;
	private int noServers;
	
	public Scheduler(int noServers) {
		servers=new LinkedList<Server>();
		for(int i=0;i<noServers;i++)
		{
			Server s=new Server();
			servers.add(s);
			Thread t=new Thread(s);
			t.start();
		}
	}
	
	public int dispatchTask(Task t)
	{
		Server min=servers.get(0);
		int index=0;
		int imax=0;
		for(Server i:servers)
		{
			if(i.getTasksSize()<min.getTasksSize()) {
				min=i;
				imax=index;
			}
			index++;
		}
		min.addTask(t);
		return imax;
	}
}
