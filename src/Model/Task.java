package Model;

public class Task {
	private int arrivalTime;
	private boolean finished;
	private int processingPeriod;
	private int serverIndex;
	
	

	public int getArrivalTime() {
		return arrivalTime;
	}

	public int getProcessingPeriod() {
		return processingPeriod;
	}

	public Task(int arrivalTime,int processingPeriod)
	{
		this.arrivalTime=arrivalTime;
		this.processingPeriod=processingPeriod;
		this.setFinished(false);
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public int getServerIndex() {
		return serverIndex;
	}

	public void setServerIndex(int serverIndex) {
		this.serverIndex = serverIndex;
	}

}
