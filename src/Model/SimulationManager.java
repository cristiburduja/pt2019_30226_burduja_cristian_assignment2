package Model;

import java.util.LinkedList;
import java.util.List;

import View.SimulationFrame;

public class SimulationManager implements Runnable{

	private int minProcessingTime;
	private int maxProcessingTime;
	private int timeLimit;
	private int numberOfServers;
	private int minArrivalTime;
	private int maxArrivalTime;
	
	private Scheduler scheduler;
	private SimulationFrame frame;
	private List<Task> generatedTasks;

	public SimulationManager(int minProcessingTime,int maxProcessingTime,int timeLimit,int numberOfServers,int minArrivalTime,int maxArrivalTime)
	{
		this.minProcessingTime=minProcessingTime;
		this.maxProcessingTime=maxProcessingTime;
		this.timeLimit=timeLimit;
		this.numberOfServers=numberOfServers;
		this.minArrivalTime=minArrivalTime;
		this.maxArrivalTime=maxArrivalTime;
		scheduler=new Scheduler(numberOfServers);
		frame=new SimulationFrame(numberOfServers);
		generatedTasks=new LinkedList<Task>();
		generateRandomTasks();
	}
	
	private void generateRandomTasks() {
		int at;
		int pt;
		for(at=0;at<=timeLimit;at+=minArrivalTime + (int)(Math.random()*(maxArrivalTime-minArrivalTime+1))){
			pt=minProcessingTime + (int)(Math.random()*(maxProcessingTime-minProcessingTime+1));
			Task t=new Task(at,pt);
			generatedTasks.add(t);
		}
	}

	@Override
	public synchronized void run() {
		int currentTime = 0;
		while(currentTime<timeLimit)
		{
			int index=0;
			//System.out.println(generatedTasks.size());
			for(Task i:generatedTasks)
			{
				if(i.getArrivalTime()==currentTime)
				{
					int serverIndex = scheduler.dispatchTask(i);
					i.setServerIndex(serverIndex);
					System.out.println("Task " + index + " was added in server " + serverIndex);
					frame.addTask(serverIndex);
				}
				if(i.isFinished()==true)
				{
					System.out.println("Task " + index + " finised");
					i.setFinished(false);
					frame.removeTask(i.getServerIndex());
				}
				index++;
			}
			currentTime++;
			try {
				this.wait(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
}
