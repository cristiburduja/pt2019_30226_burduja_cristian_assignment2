package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import Model.SimulationManager;


public class StartGui extends JFrame{
	
	
	
	private JTextField minArrivalTime;
	private JTextField maxArrivalTime;
	private JTextField minProcessingTime;
	private JTextField maxProcessingTime;
	private JTextField numberOfServers;
	private JTextField timeLimit;
	private JButton startButton;
	private JPanel panel;


	public StartGui()
	{
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(250,500);
		setupComponents();
		this.setVisible(true);
		this.add(panel);
	}
	
	public void setupComponents() {
		panel=new JPanel();
		panel.setLayout(null);
		
		minProcessingTime=new JTextField("Min processing");
		maxProcessingTime=new JTextField("Max processing");
		numberOfServers=new JTextField("Number of queues");
		timeLimit=new JTextField("Simulation time");
		minArrivalTime=new JTextField("Min arriving");
		maxArrivalTime=new JTextField("Max arriving");
		
		startButton=new JButton("Start");
		minProcessingTime.setBounds(50, 30, 120, 30);
		maxProcessingTime.setBounds(50, 80, 120, 30);
		numberOfServers.setBounds(50, 130, 120, 30);
		timeLimit.setBounds(50, 180, 120, 30);
		minArrivalTime.setBounds(50, 230, 120, 30);
		maxArrivalTime.setBounds(50, 280, 120, 30);
		startButton.setBounds(50, 330, 120, 120);

		startButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
				SimulationManager sm=new SimulationManager(Integer.parseInt(minProcessingTime.getText()), Integer.parseInt(maxProcessingTime.getText()),Integer.parseInt(timeLimit.getText()),
						Integer.parseInt(numberOfServers.getText()), Integer.parseInt(minArrivalTime.getText()), Integer.parseInt(maxArrivalTime.getText()));
				Thread t=new Thread(sm);
				t.start();
				}
				catch(Exception ex) {
					JOptionPane.showMessageDialog(panel, "Datele nu sunt corecte", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		panel.add(minArrivalTime);
		panel.add(maxArrivalTime);
		panel.add(minProcessingTime);
		panel.add(maxProcessingTime);
		panel.add(numberOfServers);
		panel.add(timeLimit);
		panel.add(startButton);
		
	}
}
