package Model;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Server implements Runnable{

	private BlockingQueue<Task> tasks;
	private boolean processingTask;
	
	public Server() {
		tasks=new LinkedBlockingQueue<Task>();
	}
	
	public void addTask(Task newTask)
	{
		try {
			tasks.put(newTask);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public synchronized void run() {
		while(true) {
			try {
				processingTask=false;
				Task t=tasks.take();
				processingTask=true;
				//System.out.println(t.getArrivalTime());
				this.wait(t.getProcessingPeriod()*1000);
				t.setFinished(true);
				//remove from gui
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
	
	public int getTasksSize() {
		if(processingTask==false)
			return tasks.size();
		else
			return 1+tasks.size();
	}
	

}
