package View;

import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

import Model.*;


public class SimulationFrame extends JFrame{
	
	private JFrame frame;
	private JPanel panel;
	private JLabel[] server;
	private LinkedList<JLabel>[] task;
	
	public SimulationFrame(int numberOfServers) {
		
		this.setSize(300,700);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		server=new JLabel[numberOfServers];
		task=new LinkedList[numberOfServers];
		
		panel = new JPanel();
		panel.setLayout(null);
		for(int i=0;i<numberOfServers;i++)
		{
			task[i]=new LinkedList<JLabel>();
			server[i]=new JLabel("server" + (i+1));
			server[i].setBounds(10+60*i, 30, 50, 20);
			panel.add(server[i]);
		}
		
		this.setContentPane(panel);
		this.setVisible(true);
	}
	
	public void addTask(int serverIndex) {
		JLabel newTask=new JLabel("Task"); //creez un text "task nou" 
		task[serverIndex].add(newTask); //il adaug in lista cu "taski"
		newTask.setBounds(10+60*serverIndex, 30+task[serverIndex].size()*30, 40, 20); //ii setez pozitia pe ecran
		panel.add(task[serverIndex].getLast()); //il pun pe ecran
		this.setContentPane(panel); //dau un "refresh" la ecran
	}
	
	public void removeTask(int serverIndex)
	{
		panel.remove(task[serverIndex].getLast()); //elimin ultimul task din coada(e irelevant ca pleaca primul pt ca aici doar ce se vede pe ecran e)
		task[serverIndex].removeLast(); //il scot si din lista de text "taski"
		this.setContentPane(panel); 
	}
	
}
